<?php
declare(strict_types=1);

namespace FiliWP\WP_Plugin_API_Checker;

use Exception;
use WP_Error;

class WP_Plugin_API_Checker
{
    private array $plugin_api_data = array();
    private array $plugin_errors = array();
    private array $plugin_notices = array();
    private ?bool $plugin_up_to_date;

    public function __construct(
        private string $plugin_slug,
        private string $plugin_version,
        private int $maximum_days_of_last_update = 365,
        private int $test_up_to_major_wp_versions = 3
    ) {
        $this->plugin_slug = $plugin_slug;
        $this->plugin_version = $plugin_version;
        $this->maximum_days_of_last_update = $maximum_days_of_last_update;
        $this->test_up_to_major_wp_versions = $test_up_to_major_wp_versions;

        try {
            $this->plugin_api_data = $this->fetch_plugin_data_from_api($this->plugin_slug);
            $this->run_checks();
        } catch (Exception $e) {
            $this->add_error('api_error', $e->getMessage());
        }
    }

    /**
     * Fetches plugin data from the WordPress API based on the provided slug name.
     *
     * @param string $slug_name The slug name of the plugin.
     * @return array The plugin data retrieved from the API.
     * @throws Exception If the plugin API returns an invalid response or no/invalid plugin data.
     */
    private function fetch_plugin_data_from_api(string $slug_name): ?array
    {
        $response = wp_remote_get("https://api.wordpress.org/plugins/info/1.0/$slug_name.json");

        $response_code = wp_remote_retrieve_response_code($response);

        if ($response_code !== 200) {
            throw new Exception('The WordPress plugin API returned an invalid response or the plugin does not exist in the plugins directory.');
        }

        $response_body = wp_remote_retrieve_body($response) ? json_decode(wp_remote_retrieve_body($response), true) : null;

        if (!isset($response_body['slug'])) {
            throw new Exception('The plugin API returned no or invalid plugin data.');
        }

        return $response_body;
    }

    /**
     * Retrieves the current version of the plugin from the API data.
     *
     * @return string The current version of the plugin.
     */
    private function get_plugin_api_version(): ?string
    {
        return $this->plugin_api_data['version'] ?? null;
    }

    /**
     * Fetches and sorts the numeric versions from the plugin API data.
     *
     * @return array The sorted numeric versions.
     */
    private function fetch_and_sort_numeric_versions_from_api(): array
    {
        $api_data = $this->plugin_api_data;

        if (isset($api_data['error']) && !empty($api_data['error'])) {
            return array();
        }

        $api_versions = $api_data['versions'] ?? array();

        if (!$api_versions) {
            return array();
        }

        $filtered_api_versions = array_filter(
            $api_versions,
            function ($key) {
                return preg_match('/^\d+(\.\d+)*$/', $key);
            },
            ARRAY_FILTER_USE_KEY
        );

        uksort(
            $filtered_api_versions,
            function ($a, $b) {
                return version_compare($b, $a);
            }
        );

        return $filtered_api_versions;
    }

    /**
     * Checks if the plugin slug matches the slug from the WordPress plugin API.
     */
    private function validate_slug_with_api()
    {
        $slug = $this->get_plugin_slug();
        $api_slug = $this->plugin_api_data['slug'] ?? null;

        if ($slug !== $api_slug) {
            $this->add_error('slug', sprintf('The installed plugin slug (%s) is different from the plugin API slug (%s) which can lead to critical issues.', $slug, $api_slug));
        }
    }

    private function validate_installed_plugin_version()
    {
        $installed_plugin_version = $this->get_plugin_version();

        if (!$installed_plugin_version) {
            $this->add_error('no_installed_version', 'No installed version found.');
        }
    }

    private function check_installed_plugin_version_higher_than_api_version()
    {
        $installed_plugin_version = $this->get_plugin_version();
        $api_plugin_version = $this->get_plugin_api_version();

        if ($installed_plugin_version > $api_plugin_version) {
            $this->add_error('installed_version_higher_than_api_version', sprintf('Installed version (%s) is higher than the API version (%s).', esc_attr($installed_plugin_version), esc_attr($api_plugin_version)));
        }
    }

    private function check_plugin_last_updated()
    {
        $date_last_updated_api = $this->plugin_api_data['last_updated'] ?? null;

        if (!$date_last_updated_api) {
            $this->add_notice('no_last_updated_date', 'The plugin has no last updated date.');
            return;
        }

        $difference = time() - strtotime($date_last_updated_api);
        $days_last_updated = round($difference / (60 * 60 * 24), 0);

        $maximum_days_last_update = $this->get_maximum_days_last_update();

        if ($days_last_updated > $maximum_days_last_update) {
            $this->add_notice('days_last_updated', sprintf('Plugin was not updated within the requested maximum of %d days.', $maximum_days_last_update));
        }
    }

    private function check_tested_up_to_major_versions()
    {
        $api_tested_up_to_date = $this->plugin_api_data['tested'] ?? null;

        if (!$api_tested_up_to_date) {
            $this->add_error('api', 'The plugin has no tested up to data (available).');
        }

        $wp_versions = wp_remote_get('https://api.wordpress.org/core/version-check/1.7/');

        if (is_wp_error($wp_versions)) {
            $this->add_error('api', $wp_versions->get_error_message());
        }

        $wp_versions = json_decode(wp_remote_retrieve_body($wp_versions), true);

        $versions = array_column($wp_versions['offers'], 'current');
        $latestVersions = array_slice($versions, 1, $this->get_test_up_to_major_wp_versions());

        if (!in_array($api_tested_up_to_date, $latestVersions)) {
            $this->add_error('api', sprintf('The plugin has not been tested with the latest %d major WordPress versions.', $this->get_test_up_to_major_wp_versions()));
        }

        return true;
    }

    /**
     * Adds an error message to the specified key in the errors array.
     *
     * @param string $key The key to add the error message to.
     * @param string $error The error message to add.
     * @return void
     */
    private function add_error($key, $error)
    {
        $this->plugin_errors[$key] = $error;
    }

    private function add_notice($key, $notice)
    {
        $this->plugin_notices[$key] = $notice;
    }

    private function add_plugin_info($key, $success)
    {
        $this->plugin_info[$key] = $success;
    }

    public function get_notices()
    {
        return $this->plugin_notices;
    }

    private function validate_plugin_versions()
    {
        $plugin_version = $this->get_plugin_version();
        $plugin_api_version = $this->get_plugin_api_version();

        if (!$plugin_version || !$plugin_api_version) {
            $this->add_error('no_plugin_version', 'No current or latest plugin version found.');
        }
    }

    private function check_for_new_version()
    {
        $plugin_version = $this->get_plugin_version();
        $plugin_api_version = $this->get_plugin_api_version();

        if (version_compare($plugin_version, $plugin_api_version, '<')) {
            $this->add_notice('new_version_available', sprintf('There is a new version of the plugin available: %s', $plugin_api_version));
        }
    }

    private function check_plugin_is_up_to_date(): void
    {
        if(!$this->get_plugin_api_version()) {
            $this->plugin_up_to_date = null;
            return;
        }

        $this->plugin_up_to_date = version_compare(
            $this->get_plugin_version(),
            $this->get_plugin_api_version(),
            '='
        );
    }

    private function check_plugin_version(): void
    {
        $this->validate_plugin_versions();
        $this->check_for_new_version();
        $this->check_plugin_is_up_to_date();
    }

    /**
     * Checks the plugin by performing various checks such as checking the plugin slug with the API,
     * checking the plugin version in the API, and checking the installed version of the plugin with the API.
     *
     * @return void
     */
    private function run_checks(): void
    {
        $this->validate_slug_with_api();
        $this->validate_installed_plugin_version();
        $this->check_installed_plugin_version_higher_than_api_version();
        $this->check_tested_up_to_major_versions();
        $this->check_plugin_version();
        $this->check_plugin_last_updated();
        $this->check_required_php_version();
        $this->check_required_wp_version();
        $this->check_plugin_is_up_to_date();
    }

    /**
     * Retrieves the slug of the plugin.
     *
     * @return string The slug of the plugin.
     */
    public function get_plugin_slug(): string
    {
        return $this->plugin_slug;
    }

    /**
     * Get the version of the plugin.
     *
     * @return string The version of the plugin.
     */
    public function get_plugin_version(): ?string
    {
        return $this->plugin_version;
    }

    private function get_maximum_days_last_update(): int
    {
        return $this->maximum_days_of_last_update;
    }

    private function get_test_up_to_major_wp_versions(): int
    {
        return $this->test_up_to_major_wp_versions;
    }

    /**
     * Returns the errors array.
     *
     * @return array The errors array.
     */
    public function get_errors(): array
    {
        return $this->plugin_errors;
    }


    public function get_plugin_api_data(): array
    {
        return $this->plugin_api_data;
    }

    private function check_required_php_version(): void
    {
        $required_api_php_version = $this->plugin_api_data['requires_php'] ?? null;
        $current_php_version = phpversion() ?? null;

        if (!$required_api_php_version || !is_string($required_api_php_version)) {
            $this->add_error('no_required_php_version', 'No valid required PHP version found.');
        }

        if (!$current_php_version || !is_string($current_php_version)) {
            $this->add_error('no_current_php_version', 'No valid current PHP version found.');
        }

        if (!$required_api_php_version || !$current_php_version) {
            return;
        }

        if (version_compare($current_php_version, $required_api_php_version, '<')) {
            $this->add_error('upgrade_php_version', sprintf('The plugin requires PHP version %s or higher. Your current PHP version is %s.', $required_api_php_version, $current_php_version));
        }
    }

    private function check_required_wp_version()
    {
        $required_wp_version = $this->plugin_api_data['requires'] ?? null;

        global $wp_version;
        $current_wp_version = $wp_version;

        if (!$required_wp_version || !is_string($required_wp_version)) {
            $this->add_error('no_required_wp_version', 'No valid required WordPress version found.');
            return;
        }

        if (version_compare($current_wp_version, $required_wp_version, '<')) {
            $this->add_error('update_wp_version', sprintf('The plugin requires WordPress version %s or higher. Your current WordPress version is %s.', $required_wp_version, $current_wp_version));
        }
    }
}
